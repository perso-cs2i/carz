<?php

namespace App\Controller;

use App\Entity\Voiture;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends AbstractController
{
    /**
     * @Route("/default", name="default")
     */
    public function index()
    {
        return $this->render('default/index.html.twig', [
            'controller_name' => 'DefaultController',
        ]);
    }

    public function services()
    {
        return $this->render('default/services.html.twig');
    }

    public function voitures()
    {
        // liste voitures
        $voitures = $this->lireVoitures();

        // retourne la vue
        return $this->render('default/catalogue.html.twig',
            [
               'voitures'=>$voitures
            ]
        );
    }

    public function lireVoitures()
    {
        // Instanciation des voitures

        $v1 = new Voiture('opel-zafira-2015.jpg','Opel',
            'Zafira Tourer','2.0 CDTi 110 Edition','22800',
            'Gris','0','1','2016',[]);
        $v2 = new Voiture('mini-countryman-2016.jpg','Mini',
            'Countryman','2.0 SD 143 Cooper Pack Red hot Chili VBA','20000',
            'Noir','2500','0','2018',['Climatisation']);
        $v3 = new Voiture('toyota-versos-2015.jpg','Toyota',
            'Verso','1.3 VVT-i 100 Style','15000',
            'Blanc','10000','1','2019',['Airbag','Climatisation']);
        $v4 = new Voiture('peugeot-207sw.jpg','Peugeot',
            '207 SW','1.4 VTi 95 Urban Move','9690',
            'Gris','100','0','2010',[]);

        // tableau des voitures
        $voitures = [$v1,$v2,$v3,$v4];

        return $voitures;
    }

    public function ficheVoiture($id)
    {
        $voitures = $this->lireVoitures();
        $voiture = $voitures[$id];

        return $this->render('default/voiture.html.twig',
        [
            'voiture'=>$voiture
        ]);
    }

}

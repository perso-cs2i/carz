<?php


namespace App\Entity;


class Voiture
{
    // attributs
    private $photo;
    private $marque;
    private $modele;
    private $motorisation;
    private $prix;
    private $couleur;
    private $kilometrage;
    private $neufOccasion;
    private $annee;
    private $options =[];


    // constructeur


    //getters et setters
    /**
     * Voiture constructor.
     * @param $photo
     * @param $marque
     * @param $modele
     * @param $motorisation
     * @param $prix
     * @param $couleur
     * @param $kilometrage
     * @param $neufOccasion
     * @param $annee
     * @param array $options
     */
    public function __construct($photo, $marque, $modele, $motorisation, $prix, $couleur, $kilometrage, $neufOccasion, $annee, array $options)
    {
        $this->photo = $photo;
        $this->marque = $marque;
        $this->modele = $modele;
        $this->motorisation = $motorisation;
        $this->prix = $prix;
        $this->couleur = $couleur;
        $this->kilometrage = $kilometrage;
        $this->neufOccasion = $neufOccasion;
        $this->annee = $annee;
        $this->options = $options;
    }

    /**
     * @return mixed
     */
    public function getPhoto()
    {
        return $this->photo;
    }

    /**
     * @param mixed $photo
     * @return Voiture
     */
    public function setPhoto($photo)
    {
        $this->photo = $photo;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getMarque()
    {
        return $this->marque;
    }

    /**
     * @param mixed $marque
     * @return Voiture
     */
    public function setMarque($marque)
    {
        $this->marque = $marque;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getModele()
    {
        return $this->modele;
    }

    /**
     * @param mixed $modele
     * @return Voiture
     */
    public function setModele($modele)
    {
        $this->modele = $modele;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getMotorisation()
    {
        return $this->motorisation;
    }

    /**
     * @param mixed $motorisation
     * @return Voiture
     */
    public function setMotorisation($motorisation)
    {
        $this->motorisation = $motorisation;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPrix()
    {
        return $this->prix;
    }

    /**
     * @param mixed $prix
     * @return Voiture
     */
    public function setPrix($prix)
    {
        $this->prix = $prix;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCouleur()
    {
        return $this->couleur;
    }

    /**
     * @param mixed $couleur
     * @return Voiture
     */
    public function setCouleur($couleur)
    {
        $this->couleur = $couleur;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getKilometrage()
    {
        return $this->kilometrage;
    }

    /**
     * @param mixed $kilometrage
     * @return Voiture
     */
    public function setKilometrage($kilometrage)
    {
        $this->kilometrage = $kilometrage;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getNeufOccasion()
    {
        return $this->neufOccasion;
    }

    /**
     * @param mixed $neufOccasion
     * @return Voiture
     */
    public function setNeufOccasion($neufOccasion)
    {
        $this->neufOccasion = $neufOccasion;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getAnnee()
    {
        return $this->annee;
    }

    /**
     * @param mixed $annee
     * @return Voiture
     */
    public function setAnnee($annee)
    {
        $this->annee = $annee;
        return $this;
    }

    /**
     * @return array
     */
    public function getOptions(): array
    {
        return $this->options;
    }

    /**
     * @param array $options
     * @return Voiture
     */
    public function setOptions(array $options): Voiture
    {
        $this->options = $options;
        return $this;
    }








}